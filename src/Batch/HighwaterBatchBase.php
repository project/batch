<?php

namespace Drupal\batch\Batch;

/**
 * Defines a batch that continuously queries items to process.
 */
abstract class HighwaterBatchBase extends BatchBase {

  /**
   * {@inheritDoc}
   */
  public function process(&$context) {
    $this->context = &$context;

    if (empty($context['sandbox'])) {
      $context['sandbox']['items'] = [];
      $context['sandbox']['highwater'] = NULL;
      $context['sandbox']['count'] = 0;
      $context['sandbox']['total'] = NULL;
    }

    // Get the items to process.
    list($context['sandbox']['items'], $context['sandbox']['highwater']) = $this->getItems($context['sandbox']['highwater']);
    // Highwater NULL indicates we're done.
    if ($context['sandbox']['highwater'] === NULL) {
      return;
    }
    // Get/update the count each time we pull a new set of items.
    $context['sandbox']['total'] = $this->countItems();

    while (!empty($context['sandbox']['items'])) {
      if ($item = array_shift($context['sandbox']['items'])) {
        $this->processItem($item, $context);
        $context['sandbox']['count']++;
      }
    }

    if ($this->reclaimMemory()) {
      $context['message'] .= ' - Reclaiming memory';
    }

    // Compute remaining.
    if ($context['sandbox']['total']) {
      $context['finished'] = $context['sandbox']['count'] / $context['sandbox']['total'];
      $context['finished'] = min($context['finished'], .9999);
    }
    else {
      // Unknown.
      $context['finished'] = 0;
    }
  }

  /**
   * Counts the number of items expected to be processed by the batch.
   *
   * Counting is optional, as it's only used to provide progress information as
   * the batch processes. Child classes should extend this with a numeric count
   * when possible.
   *
   * @return int|null
   *   Provides the total number of items if known, or FALSE if unknown.
   */
  protected function countItems() {
    return FALSE;
  }

  /**
   * Gets the items to be processed by the batch.
   *
   * In order to keep things light, this should be a list of IDs, etc. The batch
   * operation callback should then load the corresponding entity if needed.
   *
   * @param mixed $highwater
   *   The highwater value.
   *
   * @return array
   *   An array where the first element is the items to process, and the second
   *   element is the updated highwater value.
   */
  abstract protected function getItems($highwater = NULL);

  /**
   * Process one item from the batch.
   *
   * @param mixed $item
   *   The item to be processed.
   * @param array|\DrushBatchContext $context
   *   The batch context.
   */
  abstract protected function processItem($item, &$context);

}
