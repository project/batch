<?php

namespace Drupal\batch\Batch;

use Drupal\Core\Url;

/**
 * API interface for batches.
 */
interface BatchInterface {

  /**
   * Set the batch title.
   *
   * @param string $title
   *   The batch title.
   *
   * @return $this
   */
  public function setTitle($title);

  /**
   * Set the batch init message.
   *
   * @param string $initMessage
   *   The batch init message.
   *
   * @return $this
   */
  public function setInitMessage($initMessage);

  /**
   * Set the batch progress message.
   *
   * @param string $progressMessage
   *   The batch progress message.
   *
   * @return $this
   */
  public function setProgressMessage($progressMessage);

  /**
   * Get the batch definition.
   *
   * @return array
   *   The batch definition.
   */
  public function getBatch();

  /**
   * Set a URL for redirection upon completing the batch.
   *
   * @param \Drupal\Core\Url $url
   *   The URL for redirection.
   *
   * @return $this
   */
  public function setRedirectUrl(Url $url);

}
