<?php

namespace Drupal\batch\Batch;

/**
 * Defines a batch that processes over an enumerated list of items.
 */
abstract class EnumeratedBatchBase extends BatchBase {

  use ItemsPerProcessBatchTrait;

  /**
   * Gets the items to be processed by the batch.
   *
   * In order to keep things light, this should be a list of IDs, etc. The batch
   * operation callback should then load the corresponding entity if needed.
   *
   * @return array
   *   The items to process.
   */
  abstract protected function getItems();

  /**
   * Process one item from the batch.
   *
   * @param mixed $item
   *   The item to be processed.
   * @param array|\DrushBatchContext $context
   *   The batch context.
   */
  abstract protected function processItem($item, &$context);

  /**
   * {@inheritDoc}
   */
  public function process(&$context) {
    $this->context = &$context;

    if (empty($context['sandbox'])) {
      $context['sandbox']['items'] = $this->getItems();
      $context['sandbox']['total'] = count($context['sandbox']['items']);
    }

    $processed = 0;
    while ($processed++ < $this->itemsPerProcess) {
      if ($item = array_shift($context['sandbox']['items'])) {
        $this->processItem($item, $context);
      }

      // If none left, we're done.
      if (empty($context['sandbox']['items'])) {
        return;
      }
    }

    if ($this->reclaimMemory()) {
      $context['message'] .= ' - Reclaiming memory';
    }

    $count = count($context['sandbox']['items']);
    $context['finished'] = 1 - ($count / $context['sandbox']['total']);
    $context['finished'] = min($context['finished'], .9999);
  }

}
