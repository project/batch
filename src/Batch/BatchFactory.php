<?php

namespace Drupal\batch\Batch;

use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Factory for creating batches.
 */
class BatchFactory implements ContainerAwareInterface {

  use ContainerAwareTrait;

  /**
   * Instantiates a batch given a class name.
   *
   * @param string $class
   *   The class to instantiate.
   *
   * @return \Drupal\batch\Batch\BatchInterface
   *   The instantiated batch object.
   */
  public function getBatchFromClass($class) {
    if (!in_array(BatchInterface::class, class_implements($class))) {
      throw new \Exception($class . ' does not implement ' . BatchInterface::class);
    }
    return $class::create($this->container);
  }

}
