<?php

namespace Drupal\batch\Batch;

use Drupal\Component\Utility\Bytes;
use Drupal\Core\Cache\MemoryCache\MemoryCacheInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\DependencyInjection\DependencySerializationTrait;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Base class for implementing batches.
 */
abstract class BatchBase implements BatchInterface, ContainerInjectionInterface {

  use LoggerChannelTrait;
  use DependencySerializationTrait;
  use StringTranslationTrait;

  /**
   * The batch context.
   *
   * @var array|\DrushBatchContext $context
   */
  protected $context;

  /**
   * The entity memory cache service.
   *
   * @var \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface
   */
  protected $entityMemoryCache;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The batch init message.
   *
   * @var string
   */
  protected $initMessage;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * The batch progress message.
   *
   * @var string
   */
  protected $progressMessage;

  /**
   * The batch title.
   *
   * @var string
   */
  protected $title;

  /**
   * URL for redirection upon batch completion.
   *
   * @var \Drupal\Core\Url|null
   */
  protected $redirectUrl;

  /**
   * BatchBase constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager service.
   * @param \Drupal\Core\Cache\MemoryCache\MemoryCacheInterface $entityMemoryCache
   *   The entity memory cache service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, MemoryCacheInterface $entityMemoryCache, MessengerInterface $messenger) {
    $this->initMessage = $this->t('Initializing');
    $this->progressMessage = $this->t('Completed @current of @total.');
    $this->title = $this->t('Processing');
    $this->entityTypeManager = $entityTypeManager;
    $this->entityMemoryCache = $entityMemoryCache;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity.memory_cache'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function setTitle($title) {
    $this->title = $title;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setInitMessage($initMessage) {
    $this->initMessage = $initMessage;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function setProgressMessage($progressMessage) {
    $this->progressMessage = $progressMessage;
    return $this;
  }

  /**
   * {@inheritDoc}
   */
  public function getBatch() {
    return [
      'title' => $this->title,
      'init_message' => $this->initMessage,
      'progress_message' => $this->progressMessage,
      'operations' => $this->getBatchOperations(),
      'finished' => [$this, 'finished'],
    ];
  }

  /**
   * Gets the operations for the batch.
   *
   * @return array[]
   *   The operations for the batch.
   */
  protected function getBatchOperations() {
    return [[[$this, 'process'], []]];
  }

  /**
   * Batch operation callback.
   *
   * @param array|\DrushBatchContext $context
   *   The batch context.
   */
  abstract public function process(&$context);

  /**
   * Batch finished callback.
   *
   * @param bool $success
   *   A boolean indicating whether the batch has completed successfully.
   * @param mixed $results
   *   The value set in $context['results'] by callback_batch_operation().
   * @param array $operations
   *   If $success is FALSE, contains the operations that remained unprocessed.
   */
  public function finished($success, $results, array $operations) {
    // Override as needed for message setting, etc. and be sure to utilize this
    // return value to keep the redirection functionality.
    if (!empty($this->redirectUrl)) {
      return new RedirectResponse($this->redirectUrl->toString());
    }
  }

  /**
   * Reclaims memory.
   *
   * @return bool
   *   Indicates if memory was reclaimed.
   *
   * @see \Drupal\migrate\MigrateExecutable::attemptMemoryReclaim
   */
  protected function reclaimMemory() {
    $limit = trim(ini_get('memory_limit'));
    if ($limit == '-1') {
      $limit = Bytes::toNumber('256MB');
    }
    else {
      $limit = Bytes::toNumber($limit);
    }
    if (memory_get_usage() / $limit < .85) {
      return FALSE;
    }

    drupal_static_reset();

    // Entity storage can blow up with caches so clear them out.
    foreach ($this->entityTypeManager->getDefinitions() as $id => $definition) {
      $this->entityTypeManager->getStorage($id)->resetCache();
    }

    // Clear the entity storage memory cache.
    /** @var \Drupal\Core\Cache\MemoryCache\MemoryCache $memory_cache */
    $this->entityMemoryCache->deleteAll();

    // Run garbage collector to further reduce memory.
    gc_collect_cycles();
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function setRedirectUrl(Url $url) {
    $this->redirectUrl = $url;
    return $this;
  }

}
