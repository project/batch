<?php

namespace Drupal\batch\Batch;

/**
 * Allows setting items per processing run.
 */
trait ItemsPerProcessBatchTrait {

  /**
   * The number of items to process during on processing run.
   *
   * @var int
   */
  protected $itemsPerProcess = 25;

  /**
   * Set the number of items to process during each processing run.
   *
   * @param int $itemsPerProcess
   *   The number of items to process during each processing run.
   *
   * @return $this
   */
  public function setItemsPerProcess($itemsPerProcess) {
    $this->itemsPerProcess = $itemsPerProcess;
    return $this;
  }

}
